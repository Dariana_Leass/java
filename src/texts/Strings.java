package texts;

/**
 * Created by Даша Лис on 15.06.2017.
 */
public class Strings {
    public class SearchWord {

        private String words;
        private String text;

        public SearchWord(String words, String text) {
            this.words = words;
            this.text = text;
        }

        public String getWords() {
            return words;
        }

        public void setWords(String words) {
            this.words = words;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int words() { // Разбиение на слова текста
            int count = 0;
            String[] newText = text.toLowerCase().split("[,;:.!?\\s]+");
            for (String s:newText) {
                if(s == words){
                    count ++;
                }
            }
            return count;
        }
    }

}
