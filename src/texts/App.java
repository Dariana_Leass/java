package texts;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Даша Лис on 15.06.2017.
 */
public class App {

    public static void main(String[] args) {
        String text = "null";
        String word = "null";
        Scanner sin = new Scanner(System.in);
        System.out.println("Input word");
        word = sin.next();
        try {
            Scanner in = new Scanner(new File("text.txt"));
            text = in.nextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Strings strings = new Strings("the",text);
        System.out.println("You word " + word + " " + Strings.words());
    }
}
