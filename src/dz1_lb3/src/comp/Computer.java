package comp;

/**
 * Created by Даша Лис on 15.06.2017.
 */
public class Computer {
    private String model;
    private int capacity;
    private float weight;
    private int ram;
    private int frequency;

    public Computer(){}; //конструктор по умолчанию(пустой)
    public Computer(String model,int capacity,float weight, int ram, int frequency){ // конструктор с параметрами
        this.model = model;
        this.capacity = capacity;
        this.weight = weight;
        this.ram = ram;
        this.frequency = frequency;
    }
    public Computer(Computer computer){ // конструктор копирования
        model = computer.getModel();
        capacity = computer.getCapacity();
        weight = computer.getWeight();
        ram = computer.getRam();
        frequency = computer.getFrequency();
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) { // сетер для поля frequency
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "model='" + model + '\'' +
                ", capacity=" + capacity +
                ", weight=" + weight +
                ", ram=" + ram +
                ", frequency=" + frequency +
                '}';
    }
}
