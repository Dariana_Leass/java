package LB12;

/**
 * Created by Даша Лис on 14.06.2017.
 */
public class LB1_2 {
    private int i;
    private long l;
    private float f;
    private double d;
    private byte b;
    private short s;
    private boolean bool;
    private char ch;

    //Задание №1
    public void printInfo() {
        System.out.println("int = " + i); //0
        System.out.println("long = " + l); //0
        System.out.println("float = " + f); //0.0
        System.out.println("double = " + d); //0.0
        System.out.println("byte = " + b); //0
        System.out.println("short = " + s);//0
        System.out.println("boolean = " + bool); //false
        System.out.println("char = " + ch); //'\u0000'
    }
public void printLocal() {
    int IntLocal = 99;
    long LongLocal = 100;
    float FloatLocal = 3.14f;
    double DoubleLocal = 3.14;
    byte ByteLocal = 0;
    short ShortLocal = 2;
    boolean BooleanLocal = true;
    char CharLocal = 'h';

    System.out.println("byte :" + ByteLocal);
    System.out.println("short :" + ShortLocal);
    System.out.println("int :" + IntLocal);
    System.out.println("long :" + LongLocal);
    System.out.println("float :" + FloatLocal);
    System.out.println("double :" + DoubleLocal);
    System.out.println("boolean :" + BooleanLocal);
    System.out.println("char :" + CharLocal);
}
    //Задание №2
    public void checkFloat() {
        //float f1 = 1.; Невозможно присвоить значение типа double переменной float
        float f1 = (float)1.; //работает только в этом случае
        float f2 = 1;
        float f3 = 0x1;
        float f4 = 0b1;
        // float f5 = 1.0e1; первый случай
        float f5 = (float)1.0e1; //работает
        float f6 = 01;
    }
    //Задание №3
    public void checkShort() {
        short sh;
        sh = (int)1 + (int)2;
        System.out.println(sh);
        sh = (short)((int)12421 + 5.6); //работает только с явным преобразованием, иначе "невозможно преобразовать double в short"
        System.out.println(sh);
        sh = (short)(2.4f + 4);
        System.out.println(sh);
        short sh4 = (short)((byte)3+(short)4);
        System.out.println(sh);
        short sh5 = (short)(2.4f + 4.2d);
        System.out.println(sh);

    }
    //Задание №4
    public void Triangle(int katet1, int katet2, int hypotenuse) {
        System.out.println((katet1*katet1 + katet2*katet2 == hypotenuse*hypotenuse)? "Triangle`s rectangular" : "Triangle isn`t rectangular");
    }
    //Задание №5
    public int sumOf20() {
        int sum = 0;
        for (int i = 1; i <= 20; i++) {
            sum = sum+i;
        }
        return sum;
    }
    //Задание №6
    public int sumOfEven20() {
        int sum = 0, i = 1;
        while (i <= 20) {
            if ((i % 2) == 0) { //Если остаток от деление на 2 равен 0, значит число четное
                sum = sum+i;
            }
            else{
            i++;}
        }
        return sum;
    }
//Задание №7
    public void PrimeNum20() { //Проверяет, простое ли число
        int sum = 0;
        int num1, num2;
        for (num1 = 2; num1 <= 20; num1++){
            num2 = 0;
            for (int i = 1; i <= num1; i++){
                if (num1 % i == 0)
                    num2++;
            }
            if (num2 <= 2)
                sum = sum + num1;

        }
        System.out.println(sum);

    }

    //Задание №8
    public void numOf3 (int a, int b, int c) {
        if (((a + b) == c) || ((b + c) == a) || ((a + c) == b)) {
            System.out.println(true);
        }
    }
    //Задание №9
    public void averageAB() {
        int a = 20;
        int b = 10;
        while (a != b){
            --a;
            ++b;
        }
        System.out.println(a);

    }
    //Задание №10
    public void credit(double creditSum, int percent, int months) {
        if (months < 1 || percent < 0|| creditSum <0) {
            System.out.println("Error!");
            return;
        }
        double perMonthSum = creditSum / (double)months;
        for (int i = 0; i < months; ++i) {
            System.out.println("Per month: sum = " + perMonthSum + ", % = "
                    + ((creditSum - perMonthSum * i) * (double)percent / 100.0) / 12.0);
            //Считаем, что ставка годовая, поэтому делим на 12
            //В каждый месяц сумма по процентам уменьшается, т.к. уменьшается еще не выплаченная сумма
        }

        System.out.println("Total % = " + (creditSum * (double)percent / 100.0 / 12.0)
                * ((double)months + 1) / 2.0);
    }
}
