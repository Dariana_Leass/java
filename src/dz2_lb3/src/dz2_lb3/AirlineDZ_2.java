package dz2_lb3;
import java.util.ArrayList;
/**
 * Created by Даша Лис on 15.06.2017.
 */
public class AirlineDZ_2 {

    private ArrayList<Planes> AirParking = new ArrayList<>();
    public  AirlineDZ_2(){};

    public AirlineDZ_2(ArrayList<Planes> AirParking) {
        this.AirParking = AirParking;
    }

    public ArrayList<Planes> getAirParking() {
        return AirParking;
    }

    public void setAirParking(ArrayList<Planes> AirParking) {
        this.AirParking = AirParking;
    }

    public void setAirParking(Planes plane){
        AirParking.add(plane);
    }

    public void aboveAverageForThisAirline(){ //выше среднего по данной авиакомпании;
        int sum = 0;
        for (Planes obj: AirParking ) {
            System.out.println(obj);
            sum += obj.getCapacity();
        }
        for (Planes obj:AirParking) {
            if(obj.getCapacity()>sum){
                System.out.println(obj.toString());
            }
        }
    }

    public void belowAverageForThisAirline(){ //ниже среднего по данной авиакомпании;
        int sum = 0;
        for (Planes obj: AirParking ) {
            System.out.println(obj);
            sum += obj.getCapacity();
        }
        for (Planes obj:AirParking
                ) {
            if(obj.getCapacity()<sum){
                System.out.println(obj.toString());
            }
        }
    }
    public void aboveValue(int value){ // выше определенного значения;

        for (Planes obj:AirParking) {
            if (obj.getCapacity() > value) {
                System.out.println(obj.toString());
            }
        }
    }


    public void belowValue(int value){ //ниже определенного значения;

        for (Planes obj:AirParking) {
            if (obj.getCapacity() < value) {
                System.out.println(obj.toString());
            }
        }
    }


    @Override
    public String toString() {
        return "Airline{" +
                "AirParking=" + AirParking +
                '}';
    }
}

