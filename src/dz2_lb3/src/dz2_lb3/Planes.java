package dz2_lb3;

/**
 * Created by Даша Лис on 15.06.2017.
 */
public class Planes {
    private int Capacity;
    private String name;

    public Planes (){};
    public Planes(int Capacity, String name) {
        this.Capacity = Capacity;
        this.name = name;
    }
    public Planes(Planes planes){
        Capacity = planes.getCapacity();
        name = planes.getName();
    }

    public int getCapacity() {
        return Capacity;
    }

    public void setCapacity(int Capacity) {
        this.Capacity = Capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Planes{" +
                "Capacity=" + Capacity +
                ", name='" + name + '\'' +
                '}';
    }
}

