package pz4;

/**
 * Created by Даша Лис on 15.06.2017.
 */
public class Boss extends Employee {

    private double weekSalary;

    public double getWeekSalary() {
        return weekSalary;
    }

    public void setWeekSalary(double weekSalary) {
        this.weekSalary = weekSalary;
    }

    public Boss(String Name, double weekSalary) {
        super(Name);
        setWeekSalary(weekSalary);

    }

    @Override
    public double calculateSalary() {

        return getWeekSalary() * 4;
    }

    @Override
    public String toString() {
        return "Boss [Salary = " + getWeekSalary() + ", FirstName = "
                + getName();
    }

}
