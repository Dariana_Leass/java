package pz4;

/**
 * Created by Даша Лис on 15.06.2017.
 */
public class PieceWorker extends Employee {

    int countDetails; // ���������� ������������� �������
    int priceDetail; // �������� �� ���� ������������� �������

    public PieceWorker(String Name, int countDetails,int priceDetail) {
        super(Name);
        setCountDetails(countDetails);
        setPriceDetail(priceDetail);
    }

    public int getCountDetails() {
        return countDetails;
    }

    public void setCountDetails(int countDetails) {
        this.countDetails = countDetails;
    }

    public int getPriceDetail() {
        return priceDetail;
    }

    public void setPriceDetail(int priceDetail) {
        this.priceDetail = priceDetail;
    }

    @Override
    double calculateSalary() {
        return getCountDetails() * getPriceDetail();
    }

    @Override
    public String toString() {
        return "PieceWorker [getCountDetails = " + getCountDetails()
                + ", getPriceDetail = " + getPriceDetail()
                + ", getFirstName = " + getName() + "]";
    }

}
