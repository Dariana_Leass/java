package pz4;

/**
 * Created by Даша Лис on 15.06.2017.
 */
    public class App {

        public static void main(String[] args) {
            Total target = new Total("Epam");
            System.out.println(target.addEmployee(new Boss("Sasha", 25000)));
            System.out.println(target.addEmployee(new CommissionWorker("Kolya", 10000, 2)));
            System.out.println(target.addEmployee(new PieceWorker("Andrey", 22, 200)));
            System.out.println(target.addEmployee(new HourlyWorker("Igor",  25, 8, 5, 25)));
            System.out.println(target.getEmpList());
            target.TotalSalary();
        }
    }
