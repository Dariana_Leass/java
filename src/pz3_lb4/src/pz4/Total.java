package pz4;

/**
 * Created by Даша Лис on 15.06.2017.
 */
import java.util.ArrayList;

public class Total {

    private String name;
    private ArrayList<Employee> employeeList = new ArrayList<>();
    public Total(String string) {
        setName(name);
    }

    public boolean addEmployee(Employee emp) {
        for (Employee employee : employeeList) {
            if (employee.equals(emp)) {
                return false;

            }
        }
        employeeList.add(emp);
        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Employee> getEmpList() {
        return employeeList;
    }

    public void setEmpList(ArrayList<Employee> empList) {
        this.employeeList = empList;
    }

    public void TotalSalary() {
        double totalSalary = 0;
        for (Employee employee : employeeList) {
            totalSalary += employee.calculateSalary();
            employee.print();
        }
        System.out.println("Total salary :" + totalSalary);

    }

}
