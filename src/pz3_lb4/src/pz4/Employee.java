package pz4;

/**
 * Created by Даша Лис on 15.06.2017.
 */
    abstract public class Employee {
        private String Name;

        abstract double calculateSalary();

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public Employee(String Name) {
            setName(Name);
        }

        @Override
        public String toString() {
            return "Employee [Name]=" + Name;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result
                    + ((Name == null) ? 0 : Name.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Employee other = (Employee) obj;
            if (Name == null) {
                if (other.Name != null)
                    return false;
            } else if (!Name.equals(other.Name))
                return false;
            return true;
        }
        public void print() {
            System.out.println(toString() + " Salary " + calculateSalary());
        }

    }
