package pz4;

/**
 * Created by Даша Лис on 15.06.2017.
 */
public class HourlyWorker extends Employee {

    int NormalHour;
    int hour;
    int salaryPerHour;
    int salaryPerOverHour;

    public int getNormalHour() {
        return NormalHour;
    }

    public void setNormalHour(int NormalHour) {
        this.NormalHour = NormalHour;
    }

    public int getSalaryPerOverHour() {
        return salaryPerOverHour;
    }

    public void setSalaryPerOverHour(int salaryPerOverHour) {
        this.salaryPerOverHour = salaryPerOverHour;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getSalaryPerHour() {
        return salaryPerHour;
    }

    public void setSalaryPerHour(int salaryPerHour) {
        this.salaryPerHour = salaryPerHour;
    }

    public HourlyWorker(String Name, int NormalHour,int hour, int salaryPerHour, int salaryPerOverHour) {
        super(Name);
        setHour(hour);
        setSalaryPerHour(salaryPerHour);
        setNormalHour(hour);
        setSalaryPerOverHour(salaryPerHour);

    }

    @Override
    double calculateSalary() {
        if (hour < NormalHour) {
            return hour * salaryPerHour;
        } else {
            return (((hour - NormalHour) * salaryPerOverHour) + (NormalHour * salaryPerHour));
        }
    }

    @Override
    public String toString() {
        return "HourlyWorker [Hour = " + getHour() + ", SalaryPerHour = "
                + getSalaryPerHour() + ", FirstName = " + getName()
                + "]";
    }

}

